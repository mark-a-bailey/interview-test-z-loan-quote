package markb.interview.zloanquote;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class QuoterMainTest {

    @Test
    public void whenArgsLength0() {
        QuoterMain quoterMain = new QuoterMain();
        Exception exception = assertThrows(IllegalArgumentException.class, () -> quoterMain.main(new String[]{}));
        assertEquals("Error: wrong arguments supplied, need [market_file_path] and [loan_amount]", exception.getMessage());
    }


    @Test
    public void whenArgsNull() {
        QuoterMain quoterMain = new QuoterMain();
        Exception exception = assertThrows(IllegalArgumentException.class, () -> quoterMain.main(null));
        assertEquals("Error: wrong arguments supplied, need [market_file_path] and [loan_amount]", exception.getMessage());
    }


    @Test
    public void whenMarketDataFileDoesntExist() {
        QuoterMain quoterMain = new QuoterMain();
        Exception exception = assertThrows(IllegalArgumentException.class, () -> quoterMain.main(new String[]{"./MadeUpFileThatDoesntExist.csv", ""}));
        assertEquals("Error: MarketFile['C:\\sandboxes\\gitlab\\interview-test-z-loan-quote\\.\\MadeUpFileThatDoesntExist.csv'] does not exist", exception.getMessage());
    }


    @Test
    public void whenOnlyMarketDataFile() {
        QuoterMain quoterMain = new QuoterMain();
        Exception exception = assertThrows(IllegalArgumentException.class, () -> quoterMain.main(new String[]{"./MarketDataforExercise.csv"}));
        assertEquals("Error: wrong arguments supplied, need [market_file_path] and [loan_amount]", exception.getMessage());
    }


    @Test
    public void whenBadLoanAmount() {
        QuoterMain quoterMain = new QuoterMain();
        Exception exception = assertThrows(IllegalArgumentException.class, () -> quoterMain.main(new String[]{"./MarketDataforExercise.csv", "badNumber"}));
        assertEquals("Error: LoanAmount['badNumber'] is not a valid number", exception.getMessage());
    }

    @Test
    public void validArgs() {
        QuoterMain quoterMain = new QuoterMain();
        assertDoesNotThrow(() -> quoterMain.main(new String[]{"./MarketDataforExercise.csv", "800"}));
    }

    @ParameterizedTest(name = "{0} is an increment of £100 and less than £15,000")
    @CsvSource({
            "200",
            "400",
            "2000",
            "2300",
    })
    public void whenLoanAmountsValid(String loanAmount) {
        QuoterMain quoterMain = new QuoterMain();
        assertDoesNotThrow(() -> quoterMain.main(new String[]{"./MarketDataforExercise.csv", loanAmount}));
    }

    @ParameterizedTest(name = "{0} is an increment of £100 and less than £15,000")
    @CsvSource({
            "205",
            "401",
            "3057",
            "13029",
            "14501",
            "14901",
            "15100"
    })
    public void whenLoanAmountsInvalid(String loanAmount) {
        QuoterMain quoterMain = new QuoterMain();
        Exception exception = assertThrows(IllegalArgumentException.class, () -> quoterMain.main(new String[]{"./MarketDataforExercise.csv", loanAmount}));
    }
}
