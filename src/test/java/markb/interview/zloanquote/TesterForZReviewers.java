package markb.interview.zloanquote;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class TesterForZReviewers {


    @ParameterizedTest(name = "{0} is an increment of £100 and less than £15,000")
    @CsvSource({
            "200",
            "400",
            "2000",
            "2300",
    })
    public void whenLoanAmountsValid(String loanAmount) {
        QuoterMain quoterMain = new QuoterMain();
        assertDoesNotThrow(() -> quoterMain.main(new String[]{"./MarketDataforExercise.csv", loanAmount}));
    }
}
