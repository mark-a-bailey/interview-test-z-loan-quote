package markb.interview.zloanquote.utils;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NumberUtilsTest {

    @ParameterizedTest(name = "{0} formats to {1}")
    @CsvSource({
            "1.11,    1.1",
            "10.99,    11.0",
            "100.11,    100.1",
            "1000.5,    '1,000.5'",
            "123456.55,    '123,456.6'",
    })
    public void testDoublesOneDecimalPlace(double numToTest, String formatedNum) {
        assertEquals(formatedNum, NumberUtils.formatOneDecimalPlace(numToTest));
    }

    @ParameterizedTest(name = "{0} formats to {1}")
    @CsvSource({
            "1.11,    1.11",
            "10.99,    10.99",
            "100,    100.00",
            "1000.5,    '1,000.50'",
            "123456.55,    '123,456.55'",
    })
    public void testDoublesTwoDecimalPlace(double numToTest, String formatedNum) {
        assertEquals(formatedNum, NumberUtils.formatTwoDecimalPlace(numToTest));
    }

    @ParameterizedTest(name = "{0} formats to {1}")
    @CsvSource({
            "1.11,    1",
            "10.99,    11",
            "100,    100",
            "1000,    '1,000'",
            "123456,    '123,456'",
    })
    public void testDoubleOnlyCommas(double numToTest, String formatedNum) {
        assertEquals(formatedNum, NumberUtils.formatJustCommas(numToTest));
    }

    @ParameterizedTest(name = "{0} formats to {1}")
    @CsvSource({
            "1,    1",
            "10,    10",
            "100,    100",
            "1000,    '1,000'",
            "123456,    '123,456'",
    })
    public void testLongsOnlyCommas(long numToTest, String formatedNum) {
        assertEquals(formatedNum, NumberUtils.formatJustCommas(numToTest));
    }

}
