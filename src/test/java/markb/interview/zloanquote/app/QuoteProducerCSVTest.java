package markb.interview.zloanquote.app;

import markb.interview.zloanquote.domain.Quote;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class QuoteProducerCSVTest {

    @ParameterizedTest(name = "for borrowing {0}")
    @CsvSource({
            "200,   6.9,    6.1662791684438965,     221.98605006398026",
            "400,   6.9,    12.332558336887793,     443.9721001279605",
            "800,   6.980000263214111,    24.694362794677023,     888.9970606083729",
            "2300,   7.527825911045074,    71.5736984562977,     2576.6531444267175"
    })
    public void validLoansWithEnoughMarketSupply(long loanAmount, double annualInterestRate, double monthlyPayment, double totalRepayment) {
        QuoteProducerCSV quoteProducer = new QuoteProducerCSV(new File("./MarketDataforExercise.csv"));
        Quote quote = quoteProducer.getQuote(loanAmount);
        assertEquals(loanAmount, quote.getRequestedAmount());
        assertEquals(annualInterestRate, quote.getAnnualInterestRate());
        assertEquals(monthlyPayment, quote.getMonthlyRepayment());
        assertEquals(totalRepayment, quote.getTotalRepayment());
    }


    @ParameterizedTest(name = "for borrowing {0} when market has less supply")
    @CsvSource({
            "3000",
            "13000",
            "15000"
    })
    public void validLoansWithNotenoughMarketSupply(long loanAmount) {
        QuoteProducerCSV quoteProducer = new QuoteProducerCSV(new File("./MarketDataforExercise.csv"));
        Exception exception = assertThrows(RuntimeException.class, () -> quoteProducer.getQuote(loanAmount));
    }
}
