package markb.interview.zloanquote.domain;

public class Quote {

    private long requestedAmount;
    private double annualInterestRate;
    private double monthlyRepayment;
    private double totalRepayment;

    public Quote(long requestedAmount, double annualInterestRate, double monthlyRepayment, double totalRepayment) {
        this.requestedAmount = requestedAmount;
        this.annualInterestRate = annualInterestRate;
        this.monthlyRepayment = monthlyRepayment;
        this.totalRepayment = totalRepayment;
    }

    public long getRequestedAmount() {
        return requestedAmount;
    }

    public double getAnnualInterestRate() {
        return annualInterestRate;
    }

    public double getMonthlyRepayment() {
        return monthlyRepayment;
    }

    public double getTotalRepayment() {
        return totalRepayment;
    }
}
