package markb.interview.zloanquote.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Lender {
    @JsonProperty("Lender")
    private String lenderName;

    @JsonProperty("Rate")
    private double rate;

    @JsonProperty("Available")
    private long loanAvailable;

    public Lender() {
        // no arg constructor for Jackson CSV Mapper
    }

    public String getLenderName() {
        return lenderName;
    }

    public double getRate() {
        return rate;
    }

    public long getLoanAvailable() {
        return loanAvailable;
    }
}
