package markb.interview.zloanquote.view;

import markb.interview.zloanquote.domain.Quote;
import markb.interview.zloanquote.utils.NumberUtils;

public class QuoteCommandLinePrinter {

    public static void printQuote(Quote quote) {
        System.out.printf("Requested amount: £%s\n", NumberUtils.formatJustCommas(quote.getRequestedAmount()));
        System.out.printf("Annual Interest Rate: %s%%\n", NumberUtils.formatOneDecimalPlace(quote.getAnnualInterestRate())); // '%' sign escapes '%' 0_o
        System.out.printf("Monthly Repayment: £%s\n", NumberUtils.formatTwoDecimalPlace(quote.getMonthlyRepayment()));
        System.out.printf("Total Repayment: £%s\n", NumberUtils.formatTwoDecimalPlace(quote.getTotalRepayment()));
    }
}