package markb.interview.zloanquote.app;

import markb.interview.zloanquote.domain.Quote;

import java.io.File;

public class QuoteProducerCSV implements QuoteProducer {

    private LoansFromLendersFacade loansFromLendersFacade;

    public QuoteProducerCSV(File marketFile) {
        this.loansFromLendersFacade = new LoansFromLendersFacadeCsv(marketFile);
    }

    @Override
    public Quote getQuote(long loanAmount) {
       return loansFromLendersFacade.getQuoteFromLenders(loanAmount);
    }
}
