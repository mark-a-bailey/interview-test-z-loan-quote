package markb.interview.zloanquote.app;

import markb.interview.zloanquote.domain.Quote;

public interface LoansFromLendersFacade {

    public Quote getQuoteFromLenders(long loanAmount);
}
