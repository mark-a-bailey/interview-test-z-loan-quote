package markb.interview.zloanquote.app;

import markb.interview.zloanquote.domain.Quote;

public interface QuoteProducer {

    public Quote getQuote(long loanAmount);
}
