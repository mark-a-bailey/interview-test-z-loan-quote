package markb.interview.zloanquote.app;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import markb.interview.zloanquote.domain.Lender;
import markb.interview.zloanquote.domain.Quote;
import markb.interview.zloanquote.utils.AmortizationUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LoansFromLendersFacadeCsv implements LoansFromLendersFacade {

    private List<Lender> marketLenders = new ArrayList<>();
    private long currentTotalMarketLoansAvailable = 0;

    public LoansFromLendersFacadeCsv(File marketFile) {
        try {
            initMarketLendersList(marketFile);
        } catch (IOException e) {
            throw new RuntimeException("");
        }
    }

    private void initMarketLendersList(File marketFile) throws IOException {
        CsvMapper csvMapper = new CsvMapper();
        CsvSchema schema = CsvSchema.emptySchema().withHeader();

        ObjectReader oReader = csvMapper.reader(Lender.class).with(schema);

        try (Reader reader = new FileReader(marketFile)) {
            MappingIterator<Lender> mi = oReader.readValues(reader);
            while (mi.hasNext()) {
                Lender current = mi.next();
                marketLenders.add(current);
                currentTotalMarketLoansAvailable += current.getLoanAvailable();
            }
        }

        //pre sort the lenders to the lowest rates so that borrowers get the best deal
        Collections.sort(marketLenders, (o1, o2) -> {return Double.compare(o1.getRate(), o2.getRate());});
    }


    @Override
    public Quote getQuoteFromLenders(long loanAmount) {
        if (loanAmount>currentTotalMarketLoansAvailable) {
            throw new RuntimeException("Error: Market supply does not have enough offers to fulfil this request, please try again later");
        }

        double annualInterestRateDecimal = 0.0;
        long loopedLoanAmount =0;

        for (Lender marketLender : marketLenders) {
            //if we still need more money from lenders, add it to running total
            if (loopedLoanAmount < loanAmount) {
                //only borrow what we need
                long loanAmountToAdd = Math.min(marketLender.getLoanAvailable(), loanAmount-loopedLoanAmount);
                loopedLoanAmount += loanAmountToAdd;
                float percentageOfLoan = ((float)loanAmountToAdd / loanAmount)*100;
                annualInterestRateDecimal += (percentageOfLoan*marketLender.getRate());
            }
        }
        double decimalAnnualPercentageRate = (annualInterestRateDecimal/100);
        double monthlyPayment = AmortizationUtils.getPeriodicPaymentAmount(loanAmount, decimalAnnualPercentageRate/12, 36);
        double totalRepayment = monthlyPayment * 36;

        return new Quote(loanAmount, annualInterestRateDecimal, monthlyPayment, totalRepayment);
    }
}
