package markb.interview.zloanquote;

import markb.interview.zloanquote.app.QuoteProducer;
import markb.interview.zloanquote.app.QuoteProducerCSV;
import markb.interview.zloanquote.domain.Quote;
import markb.interview.zloanquote.view.QuoteCommandLinePrinter;

import java.io.File;

public class QuoterMain {

    public static final int ARG_POSITION_MARKET_FILE = 0;
    public static final int ARG_POSITION_LOAN_AMOUNT = 1;

    public static void main(String[] args) {
        if (args==null || args.length!=2) {
            throw  new IllegalArgumentException("Error: wrong arguments supplied, need [market_file_path] and [loan_amount]");
        }

        File marketFile = validateAndGetMarketFile(args[ARG_POSITION_MARKET_FILE]);
        long loanAmount = validateAndGetLoanAmount(args[ARG_POSITION_LOAN_AMOUNT]);

        QuoteProducer quoteProducer = new QuoteProducerCSV(marketFile);
        Quote quote = quoteProducer.getQuote(loanAmount);

        QuoteCommandLinePrinter.printQuote(quote);
    }

    private static File validateAndGetMarketFile(String filePath) {
        File marketFile = new File(filePath);

        if (!marketFile.exists()) {
            throw new IllegalArgumentException("Error: MarketFile['"+ marketFile.getAbsolutePath()+"'] does not exist");
        }
        return marketFile;
    }

    private static long validateAndGetLoanAmount(String loanAmountArg) {
        long loanAmount = 0;
        try {
            loanAmount = Long.parseLong(loanAmountArg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Error: LoanAmount['"+ loanAmountArg+"'] is not a valid number");
        }

        if (loanAmount % 100 != 0) {
            throw new IllegalArgumentException("Error: LoanAmount['"+ loanAmountArg+"'] must be a £100 increment");
        }

        if (loanAmount > 15000) {
            throw new IllegalArgumentException("Error: LoanAmount['"+ loanAmountArg+"'] must be less than 15000");
        }

        return loanAmount;
    }
}
