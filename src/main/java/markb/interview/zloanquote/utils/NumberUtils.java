package markb.interview.zloanquote.utils;


public class NumberUtils {

    public static String formatOneDecimalPlace(Number number) {
        return String.format("%,.1f", number);
    }

    public static String formatTwoDecimalPlace(Number number) {
        return String.format("%,.2f", number);
    }

    public static String formatJustCommas(Number number) {
        if (number instanceof Long) {
            return String.format("%,d", number);
        }
        return String.format("%,.0f", number);
    }
}
