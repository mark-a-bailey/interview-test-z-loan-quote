package markb.interview.zloanquote.utils;

public class AmortizationUtils {

    public static double getPeriodicPaymentAmount(long loanAmount, double periodicInterestRate, int numberOfPayments) {
        return loanAmount * (
                periodicInterestRate / (
                    1 - (
                        Math.pow(1/(1+periodicInterestRate), numberOfPayments)
                    )
                )
        );
    }
}
